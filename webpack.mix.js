let path = require('path');
let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.config.vue.esModule = true;

mix
    .disableNotifications()
    .options({
        extractVueStyles: false,
        uglify: {
            uglifyOptions: {
                compress: {
                    drop_console: true,
                }
            }
        }
    })
    .setPublicPath('public')
    .js('resources/assets/javascripts/application.js', 'public/javascripts')
    .sass('resources/assets/stylesheets/application.scss', 'public/stylesheets')
    .copy('resources/assets/images', 'public/images')
    .copy('resources/assets/fonts', 'public/fonts')
    .sourceMaps()
    .browserSync({
        proxy: 'amv.test',
        notify: false,
        open: false,
    });

if (mix.inProduction()) {
    mix.version();

    mix.extract([
        'vue',
        'axios',
        'vuex',
        'jquery',
        'popper.js',
        'vue-meta',
        'js-cookie',
        'bootstrap',
        'vue-router',
    ])
}

mix.webpackConfig({
    plugins: [],
    resolve: {
        extensions: ['.js', '.json', '.vue'],
        alias: {
            '~': path.join(__dirname, './resources/assets/javascripts')
        }
    },
    output: {
        chunkFilename: 'javascripts/[name].js?id=[chunkhash]',
        publicPath: mix.config.hmr ? '//localhost:8080' : '/'
    }
});

