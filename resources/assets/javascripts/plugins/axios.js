import Vue from 'vue';
import axios from 'axios'
import store from '~/store'
import router from '~/router'

Vue.prototype.$http = axios.create();

axios.interceptors.request.use(request => {
    const token = store.getters['auth/token'];
    if (token) {
        request.headers.common['Authorization'] = `Bearer ${token}`;
    }

    return request;
});

axios.interceptors.response.use(response => response, error => {
    const {status} = error.response;

    if (status >= 500) {
        alert('Что-то пошло не так! Попробуйте повторить позже.');
    }

    if (status === 401 && store.getters['auth/check']) {
        alert('Пожалуйста, войдите в свой аккаунт, чтобы продолжить.').then(() => {
            store.commit('auth/LOGOUT');

            router.push({name: 'login'});
        })
    }

    return Promise.reject(error);
});
