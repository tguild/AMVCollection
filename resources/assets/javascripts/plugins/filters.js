import Vue from 'vue';
import moment from 'moment';

Vue.filter('formatDate', function (date) {
    return moment(date).format('D MMM YYYY');
});

Vue.filter('fromNow', function (datetime) {
    return moment(datetime).fromNow();
});

Vue.filter('HHMMSS', function (duration) {
    let hours   = Math.floor(duration / 3600);
    return hours === 0 ?
        moment().startOf('day').seconds(duration).format('mm:ss') : moment().startOf('day').seconds(duration).format('H:mm:ss');
});

Vue.filter('capitalize', function (value) {
    if (!value) return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1)
});