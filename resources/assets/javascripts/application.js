import Vue from 'vue';
import Application from '~/components/Application';
import router from '~/router';
import store from '~/store';

import '~/components';
import '~/plugins';

Vue.config.productionTip = false;

window.EventBus = new Vue({
    name: 'EventBus',
});

new Vue({
    router,
    store,
    ...Application,
});
