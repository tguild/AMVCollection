import Vue from 'vue';
import VueRouter from 'vue-router';
import VueMeta from 'vue-meta';
import routes from './routes';
import store from '~/store';
import {sync} from 'vuex-router-sync';

Vue.use(VueRouter);
Vue.use(VueMeta);

const globalMiddleware = ['check-auth'];

const routeMiddleware = resolveMiddleware(
    require.context('~/middleware', false, /.*\.js$/)
);

const router = create();

sync(store, router);

export default router;

function create() {
    const router = new VueRouter({
        mode: 'history',
        routes: routes,
    });

    router.beforeEach(beforeEach);
    router.afterEach(afterEach);

    return router;
}

async function beforeEach(to, from, next) {
    const components = await resolveComponents(
        router.getMatchedComponents({...to})
    );

    if (components.length === 0) {
        return next();
    }

    if (components[components.length - 1].loading !== false) {
        router.app.$nextTick(() => router.app.$loading.start());
    }

    const middleware = getMiddleware(components);

    callMiddleware(middleware, to, from, (...args) => {
        next(...args);
    })
}

async function afterEach(to, from, next) {
    await router.app.$nextTick();

    router.app.$loading.finish();
}

function resolveComponents(components) {
    return Promise.all(components.map(component => {
        return typeof component === 'function' ? component() : component;
    }))
}

function resolveMiddleware(requireContext) {
    return requireContext.keys()
        .map(file => [file.replace(/(^.\/)|(\.js$)/g, ''), requireContext(file)])
        .reduce((guards, [name, guard]) => (
            {...guards, [name]: guard.default,}
        ), {});
}

function getMiddleware(components) {
    const middleware = [...globalMiddleware];

    components.filter(c => c.middleware).forEach(component => {
        if (Array.isArray(component.middleware)) {
            middleware.push(...component.middleware);
        } else {
            middleware.push(component.middleware);
        }
    });

    return middleware;
}

function callMiddleware(middleware, to, from, next) {
    const stack = middleware.reverse();

    const _next = (...args) => {
        if (args.length > 0 || stack.length === 0) {
            if (args.length > 0) {
                router.app.$loading.finish();
            }

            return next(...args);
        }

        const middleware = stack.pop();

        if (typeof middleware === 'function') {
            middleware(to, from, _next);
        } else if (routeMiddleware[middleware]) {
            routeMiddleware[middleware](to, from, _next);
        } else {
            throw Error(`Undefined middleware [${middleware}]`);
        }
    };

    _next();
}