const Index = () => import(/* webpackChunkName: "index" */ '~/pages/Index').then(m => m.default || m);
const Watch = () => import(/* webpackChunkName: "watch" */ '~/pages/Watch').then(m => m.default || m);

const AuthIndex = () => import(/* webpackChunkName: "auth" */ '~/pages/Auth/Index').then(m => m.default || m);
const AuthLogin = () => import(/* webpackChunkName: "auth" */ '~/pages/Auth/Login').then(m => m.default || m);
const AuthRegister = () => import(/* webpackChunkName: "auth" */ '~/pages/Auth/Register').then(m => m.default || m);

const Upload = () => import(/* webpackChunkName: "upload" */ '~/pages/Upload').then(m => m.default || m);

const ManageIndex = () => import(/* webpackChunkName: "manage" */ '~/pages/Manage/Index').then(m => m.default || m);
/*const ManageComments = () => import(/!* webpackChunkName: "manage" *!/ '~/pages/Manage/Comments/Index').then(m => m.default || m);
const ManageSubscriptions = () => import(/!* webpackChunkName: "manage" *!/ '~/pages/Manage/Subscriptions/Index').then(m => m.default || m);
const ManageVideos = () => import(/!* webpackChunkName: "manage" *!/ '~/pages/Manage/Videos/Index').then(m => m.default || m);*/

export default [
    {
        path: '/',
        name: 'index',
        component: Index,
    },
    {
        path: '/watch/:uid',
        name: 'watch',
        component: Watch,
        props: true,
    },
    {
        path: '/auth',
        component: AuthIndex,
        children: [
            {
                path: '',
                redirect: {
                    name: 'auth.login',
                },
            },
            {
                path: 'login',
                name: 'auth.login',
                component: AuthLogin,
            },
            {
                path: 'register',
                name: 'auth.register',
                component: AuthRegister,
            },
        ],
    },
    {
        path: '/upload',
        name: 'upload',
        component: Upload,
    },
    {
        path: '/manage',
        component: ManageIndex,
        children: [
            {
                path: '',
                redirect: {
                    name: 'manage.videos',
                },
            },
            {
                path: 'videos',
                name: 'manage.videos',
            }
        ],
    }
];