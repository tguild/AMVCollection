import Vue from 'vue';
import Button from './Form/Button';
import Checkbox from './Form/Checkbox';
import Spinner from './Loaders/Spinner';
import Thumbnail from './Media/Thumbnail';
import Pagination from './Form/Pagination';

[
    Button,
    Checkbox,
    Spinner,
    Thumbnail,
    Pagination,
].forEach(Component => {
    Vue.component(Component.name, Component)
});