<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="CSRF-Token" content="{{ csrf_token() }}">
        <title>AMVCollection</title>
        <link rel="stylesheet" href="{{ mix('stylesheets/application.css') }}">
    </head>
    <body>
        <div id="root"></div>
        <script src="{{ mix('javascripts/application.js') }}"></script>
    </body>
</html>
