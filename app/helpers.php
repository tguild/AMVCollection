<?php

if (!function_exists('transliterate')) {
    function transliterate($string, $type = 'text', $lowercase = false)
    {
        $replace = [
            'ж' => 'zh', 'ч' => 'ch', 'щ' => 'shh', 'ш' => 'sh',
            'ю' => 'yu', 'ё' => 'yo', 'я' => 'ya', 'э' => 'e',
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p',
            'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ъ' => '',
            'ь' => '', 'ы' => 'i',
            'Ж' => 'Zh', 'Ч' => 'Ch', 'Щ' => 'Shh', 'Ш' => 'Sh',
            'Ю' => 'Yu', 'Ё' => 'Yo', 'Я' => 'Ya', 'Э' => 'E',
            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K', 'Л' => 'L',
            'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P',
            'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ъ' => '',
            'Ь' => '', 'Ы' => 'I',
        ];

        $chars = implode('', array_keys($replace));

        $clearedString = preg_replace("/[^\\s\\.\\w${chars}]/iu", '', $string);

        $transliterated = str_replace(array_keys($replace), array_values($replace), $clearedString);

        if ('filename' === $type) {
            $transliterated = preg_replace('/\s+/', '_', $transliterated);
        } else if ('url' === $type) {
            $transliterated = preg_replace('/\s+/', '-', $transliterated);
        }

        if (true === $lowercase) {
            $transliterated = strtolower($transliterated);
        }

        return $transliterated;
    }
}