<?php

namespace App\Jobs\Video;

use App\Models\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;

class Convert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $upload;
    private $video;
    private $dimensions = [
        240 => 426,
        360 => 640,
        480 => 854,
        720 => 1280,
        1080 => 1920,
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Video $video, $upload)
    {

        $this->video = $video;
        $this->upload = $upload;
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['convert', 'video:' . $this->upload['file']];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $video = FFMpeg::fromDisk('files')->open($this->upload['storage'] . '/' . $this->upload['file']);

        $dimensions = $video->getStreams()->videos()->first()->getDimensions();
        $duration = $video->getDurationInSeconds();

        $format = new \FFMpeg\Format\Video\X264('libmp3lame', 'libx264');
        $format->on('progress', function ($video, $format, $percentage) {
            //event(new VideoConverted($percentage));
        });

        foreach ($this->dimensions as $height => $width) {
            $name = $this->upload['name'] . '_' . $height . '.mp4';

            $video->addFilter(function ($filters) use ($width, $height) {
                $filters->resize(new \FFMpeg\Coordinate\Dimension((integer)$width, (integer)$height));
            })
                ->export()
                ->inFormat($format)
                ->toDisk('files')
                ->save($this->upload['storage'] . '/' . $name);

            if ($height === 240) {
                $this->video->update([
                    'duration' => $duration,
                    '240p' => true,
                ]);
            } else {
                $this->video->update([
                    $height . 'p' => true,
                ]);
            }

            $this->video->files()->create([
                'disk' => 'files',
                'path' => $this->upload['storage'] . '/' . $name,
                'type' => 'video',
                'size' => Storage::disk('files')->size($this->upload['storage'] . '/' . $name),
            ]);

            if ($height >= $dimensions->getHeight())
                break;
        }

        $this->video->update([
            'processed' => false,
        ]);

        Storage::disk('files')->delete($this->upload['storage'] . '/' . $this->upload['file']);
    }
}
