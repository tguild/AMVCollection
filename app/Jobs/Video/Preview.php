<?php

namespace App\Jobs\Video;

use App\Models\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;

class Preview implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $video;
    protected $upload;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Video $video, $upload)
    {

        $this->video = $video;
        $this->upload = $upload;
    }

    /**
     * Execute the job.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function handle()
    {
        $video = FFMpeg::fromDisk('files')->open($this->upload['storage'] . '/' . $this->upload['file']);

        $video->getFrameFromSeconds(0)
            ->export()
            ->toDisk('files')
            ->save($this->upload['storage'] . '/' . $this->upload['name'] . '.jpg');

        shell_exec('ffmpeg -v warning -ss 0 -t 3 -i ' .
            storage_path('app/public/files/' . $this->upload['storage'] . '/' . $this->upload['file']) .
            ' -vf fps=15 -y ' .
            storage_path('app/public/files/' . $this->upload['storage'] . '/' . $this->upload['name'] . '.gif')
        );

        $preview = $this->video->files()->createMany([
            [
                'disk' => 'files',
                'path' => $this->upload['storage'] . '/' . $this->upload['name'] . '.jpg',
                'type' => 'image',
                'size' => Storage::disk('files')->size($this->upload['storage'] . '/' . $this->upload['name'] . '.jpg'),
            ],
            [
                'disk' => 'files',
                'path' => $this->upload['storage'] . '/' . $this->upload['name'] . '.gif',
                'type' => 'image',
                'size' => Storage::disk('files')->size($this->upload['storage'] . '/' . $this->upload['name'] . '.gif'),
            ]
        ]);

        return $preview;
    }
}
