<?php

namespace App\Jobs\Video;

use App\Http\Requests\VideoRequest;
use App\Models\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class Upload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $video;
    protected $file;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Video $video, VideoRequest $request)
    {
        $this->video = $video;
        $this->file = $request->file('file');
        $this->user = $request->author();
    }

    /**
     * Execute the job.
     *
     * @return array
     */
    public function handle()
    {
        $storage = $this->user->uid . '/' . $this->video->uid;

        $file = transliterate($this->file->getClientOriginalName(), 'filename', true);
        $extension = $this->file->getClientOriginalExtension();
        $name = basename($file, '.'.$extension);

        Storage::disk('files')->putFileAs($storage, $this->file, $file);

        return [
            'storage' => $storage,
            'file' => $file,
            'name' => $name,
            'extension' => $extension,
        ];
    }
}
