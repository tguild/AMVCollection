<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uid' => $this->uid,
            'name' => $this->name,
            'avatar' => $this->avatar,
            'created_at' => $this->created_at->todatetimestring(),
            'updated_at' => $this->updated_at->todatetimestring(),
        ];
    }
}
