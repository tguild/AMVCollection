<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\File as FileResource;

class Files extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        $this->collection->transform(function ($data) {
             return FileResource::collection($this->collection);
        });

        return FileResource::collection($this->collection);
    }
}
