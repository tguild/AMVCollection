<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class Video extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uid' => $this->uid,
            'title' => $this->title,
            'description' => $this->description,
            'tags' => $this->tags,
            'duration' => $this->duration,
            'processed' => $this->processed,
            'thumbnail' => $this->thumbnail,
            'preview' => $this->preview,
            'sources' => $this->sources,
            'published_at' => $this->published_at ? $this->published_at->todatetimestring() : null,
            'user' => new UserResource($this->whenLoaded('user')),
            'counters' => [
                'views' => $this->views->count(),
            ],
        ];
    }
}
