<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class File extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'disk' => $this->disk,
            'path' => $this->path,
            'type' => $this->type,
            'size' => $this->size,
        ];
    }
}
