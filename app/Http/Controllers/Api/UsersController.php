<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['subscribers', 'subscriptions']);
    }

    public function auth(Request $request)
    {
        return new UserResource($request->user());
    }

    public function subscribers(User $user)
    {
        dd($user->subscribers->toArray());
    }

    public function subscriptions(User $user)
    {
        dd($user->subscriptions->toArray());
    }
}
