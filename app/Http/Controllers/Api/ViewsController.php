<?php

namespace App\Http\Controllers\Api;

use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ViewsController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {

    }

    public function create(Video $video, Request $request)
    {
        $video->views()->create([
            'ip' => $request->ip(),
            'user_id' => $request->user() ? $request->user()->id : null,
        ]);

        return response()->json(true);
    }
}
