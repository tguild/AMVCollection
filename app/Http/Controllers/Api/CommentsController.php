<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Comments as CommentsCollection;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function index(Video $video)
    {
        $comments = $video->comments()->with(['replies', 'user'])->latestFirst()->get();
        return new CommentsCollection($comments);
    }

    public function store(Video $video, CommentRequest $request)
    {
        $comment = $video->comments()->create([
            'body' => $request->body,
            'user_id' => $request->user()->id
        ]);

        return response()->json([
            'comment' => $comment,
        ]);

    }

    public function update()
    {
        //
    }

    public function delete(Comment $comment)
    {
        $comment->delete();

        return response()->json(true);
    }
}
