<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\VideoRequest;
use App\Jobs\Video\Upload as VideoUpload;
use App\Jobs\Video\Preview as VideoPreview;
use App\Jobs\Video\Convert as VideoConvert;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Videos as VideosCollection;
use App\Http\Resources\Video as VideoResource;

class VideosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'update', 'delete']);
    }

    public function index(Request $request)
    {
        $videos = Video::published()->with(['user'])->paginate(1);
        return new VideosCollection($videos);
    }

    public function show(Video $video)
    {
        return new VideoResource($video->load('user'));
    }
    
    public function store(VideoRequest $request)
    {
        $video = new Video([
            'uid' => str_random(16),
            'title' => $request->title,
        ]);

        $video->userBy($request->author());
        $video->save();

        $upload = VideoUpload::dispatchNow($video, $request);
        $preview = VideoPreview::dispatchNow($video, $upload);
        VideoConvert::dispatch($video, $upload)->onQueue('convert');

        return response()->json([
            'video' => $video,
            'preview' => $preview,
        ]);
    }

    public function update()
    {
        
    }

    public function delete()
    {
        
    }
}
