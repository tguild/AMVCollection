<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\OAuth;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class OAuthController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        //
    }

    public function redirectToProvider($provider)
    {
        return [
            'url' => Socialite::driver($provider)->stateless()->redirect()->getTargetUrl(),
        ];
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $user = $this->findOrCreateUser($provider, $user);

        $this->guard()->setToken(
            $token = $this->guard()->login($user)
        );

        return view('oauth/callback', [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->getPayload()->get('exp') - time(),
        ]);
    }

    protected function findOrCreateUser($provider, $user)
    {
        $oauth = OAuth::where('provider', $provider)
            ->where('provider_id', $user->getId())
            ->first();

        if ($oauth) {
            $oauth->update([
                'access_token' => $user->token,
                'refresh_token' => $user->refreshToken,
            ]);

            return $oauth->user;
        }

        return $this->createUser($provider, $user);
    }

    protected function createUser($provider, $user)
    {
        $u = User::create([
            'name' => $user->getName(),
            'uid' => str_random(16),
            'email' => $user->getEmail(),
        ]);

        $u->oauth()->create([
            'provider' => $provider,
            'provider_id' => $user->getId(),
            'access_token' => $user->token,
            'refresh_token' => $user->refreshToken,
        ]);

        return $u;
    }
}
