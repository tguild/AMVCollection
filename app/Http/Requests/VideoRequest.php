<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class VideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'sometimes|required|mimes:mp4,mov,webm,mpeg,3gp,avi,flv,ogg,mkv,mk3d,mks,wmv,quicktime|max:102400',
            'title' => 'sometimes|required',
            'description' => 'nullable',
            'tags' => 'nullable',
        ];
    }

    /**
     * @return User
     */
    public function author(): User
    {
        return $this->user();
    }
}
