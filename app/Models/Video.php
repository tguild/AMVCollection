<?php

namespace App\Models;

use App\Traits\Orderable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Video extends Model
{
    use Orderable;

    protected $files;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'title', 'description', 'tags', 'duration', 'processed', '240p', '360p', '480p', '720p', '1080p', 'published_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'published_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'processed' => 'boolean',
        'allow_votes' => 'boolean',
        'allow_comments' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        '240p', '360p', '480p', '720p', '1080p',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uid';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @param User $user
     */
    public function userBy(User $user)
    {
        $this->user()->associate($user);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function files()
    {
        return $this->morphMany('App\Models\File', 'filable');
    }

    public function getTagsAttribute($value)
    {
        return $value !== null ? explode(',', $value) : [];
    }

    public function getThumbnailAttribute()
    {
        $this->files = $this->files()->get();
        return empty($this->files[0]) ? null : Storage::disk('files')->url($this->files[0]->path);
    }

    public function getPreviewAttribute()
    {
        return empty($this->files[1]) ? null : Storage::disk('files')->url($this->files[1]->path);
    }

    public function getSourcesAttribute()
    {
        $dimensions = ['240p', '360p', '480p', '720p', '1080p'];
        $sources = [];

        foreach ($dimensions as $id => $dimension) {
            if (!empty($this->files[$id + 2])) {
                $sources[] = [
                    'src' => Storage::disk('files')->url($this->files[$id + 2]->path),
                    'type' => 'video/mp4',
                    'size' => (integer)substr($dimension, 0, -1),
                ];
            }
        }

        return $sources;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function views()
    {
        return $this->hasMany('App\Models\View');
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable')->whereNull('reply_id');
    }

}
