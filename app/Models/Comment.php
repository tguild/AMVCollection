<?php

namespace App\Models;

use App\Traits\Orderable;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use Orderable;

    protected $fillable = [
        'body', 'user_id', 'reply_id',
    ];

    protected $hidden = [
        'commentable_id', 'commentable_type', 'replies_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function commentable()
    {
        return $this->morphTo();
    }

    public function replies()
    {
        return $this->hasMany('App\Models\Comment', 'reply_id', 'id')->with('user');
    }
}
