<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * Routes for everyone
 */
Route::group(['middleware' => 'api'], function () {
    Route::get('videos', 'Api\VideosController@index');
    Route::get('videos/{video}', 'Api\VideosController@show');

    Route::get('/users/{user}/subscribers', 'Api\UsersController@subscribers');
    Route::get('/users/{user}/subscriptions', 'Api\UsersController@subscriptions');

    Route::get('videos/{video}/comments', 'Api\CommentsController@index');

    Route::post('videos/{video}/views', 'Api\ViewsController@create');
});

/*
 * Routes only for guests
 */
Route::group(['middleware' => 'guest:api'], function () {
    Route::post('auth/login', 'Auth\LoginController@login');
    Route::post('auth/register', 'Auth\RegisterController@register');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});

/*
 * Routes only for authorized users
 */
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('auth/user', 'Api\UsersController@auth');

    Route::post('auth/logout', 'Auth\LoginController@logout');

    Route::post('videos', 'Api\VideosController@store');
    Route::put('videos/{video}', 'Api\VideosController@update');
    Route::delete('videos/{video}', 'Api\VideosController@delete');

    Route::post('videos/{video}/comments', 'Api\CommentsController@store');
    Route::delete('videos/{video}/comments/{comment}', 'Api\CommentsController@delete');
});
