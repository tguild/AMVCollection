# About AMVCollection

> A Laravel - Vue SPA project.

## Features

- Laravel
- Vue + VueRouter + Vuex
- Pages with dynamic import and custom layouts
- Login, register and password reset
- Authentication with JWT
- Socialite integration
- Bootstrap + Font Awesome

## Installation

- `git clone https://gitlab.com/tguild/AMVCollection.git`
- Copy `.env.example` file to `.env` and edit database connection details
- Run `php artisan key:generate` and `php artisan jwt:secret`
- `php artisan migrate`
- `yarn` / `npm install`

## Usage

#### Development

```bash
# build and watch
npm run watch

# serve with hot reloading
npm run hot
```

#### Production

```bash
npm run production
```