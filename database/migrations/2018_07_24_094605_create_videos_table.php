<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('uid', 16)->unique();
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('tags')->nullable();
            $table->integer('duration')->default(0);
            $table->boolean('processed')->default(true);
            $table->boolean('240p')->default(false);
            $table->boolean('360p')->default(false);
            $table->boolean('480p')->default(false);
            $table->boolean('720p')->default(false);
            $table->boolean('1080p')->default(false);
            $table->timestamp('published_at')->nullable();
            $table->timestamps();

            $table->index('title', 'index_videos_on_title');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
